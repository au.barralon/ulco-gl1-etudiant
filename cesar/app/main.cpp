#include <cesar/cesar.hpp>
#include "../src/cesar/cesar.cpp"
#include <iostream>

int main() {
    string str;
    cout << "Choisissez le fichier : ";
    cin >> str;
    cesar *ce = new cesar;
    ce->rempliralphabet();
    ce->getmsg(str);
    ce->decodage(str);
    return 0;
}

