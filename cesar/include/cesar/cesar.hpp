#pragma once
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>

using namespace std;

struct cesar
{
    void decodage(string);
    void getmsg(string);
    void rempliralphabet();
    void frequence();

    vector<char> lecode;
    char alphabet[26];
    char character;
    int key;
    int freq;
    double cmp=0;
    vector<double> freqCode;
    const std::vector<double> refFreqs {0.082, 0.015, 0.028, 0.043, 0.127, 0.022, 0.02, 0.061, 0.07, 0.002, 0.008, 0.04, 0.024, 0.067, 0.075, 0.019, 0.001, 0.06, 0.063, 0.091, 0.028, 0.01, 0.024, 0.002, 0.02, 0.001};
};