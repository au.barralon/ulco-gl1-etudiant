#include <cesar/cesar.hpp>
#include <fstream>
#include <iostream>
#include <cstdlib>
#include <vector>


using namespace std;

void cesar::rempliralphabet()
{
    int j = 0;
    for(char i='A'; i<='Z'; i++)
    {
        alphabet[j]=i;
        j++;
    }
    return;
}

void cesar::getmsg(string str)
{
   ifstream fichier;
   fichier.open(str);
    if(fichier.eof())
    {
       return;
    }
    while(fichier)
    {
        fichier.get(character);
        lecode.push_back(character);
        if(character != ' ')
        {
            cmp++;
        }
    }
   fichier.close();
}

void cesar::decodage(string str)
{
    ofstream Dcode;
    Dcode.open("output.txt");

    key = 3;
    for(int i=0; i<lecode.size(); i ++)
    {
        character = lecode[i];
        if(character != ' ')
        {
            character = character + key;
            if(character >'z')
            {       
                character = character - 'z' + 'a' - 1;
            }
        }
        Dcode << character;
    }
    frequence();
    Dcode.close();
}

void cesar::frequence()
{
    double stat[26];
    for(int i=0; i <26; i++)
        stat[i]=0;
    for(int i=0; i<lecode.size(); i++)
    {
        if(lecode[i] >= 'a' && lecode[i] <= 'z')
        {
            freq = lecode[i] - 97;
            stat[freq]++;
        }
    }
//    for(int i=0; i<26; i++)
//    {
//        freqCode[i] = stat[i]/100;;
//        cout << "C'est la frequence du code : " << freqCode[i] << endl;
//    }
}
