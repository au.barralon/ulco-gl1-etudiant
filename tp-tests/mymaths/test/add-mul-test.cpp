#include "add.hpp"
#include "mul.cpp"

#include <catch2/catch.hpp>

TEST_CASE( "add2 0...4", "[add2]" ) {
    REQUIRE( add2(0) == 2 );
    REQUIRE( add2(1) == 3 );
    REQUIRE( add2(2) == 4 );
    REQUIRE( add2(3) == 5 );
    REQUIRE( add2(4) == 6 );
    // REQUIRE( false );  // 
}

TEST_CASE ("addn 5 5", "[addn]" ) {
    REQUIRE( addn(5,5) == 10 );
}

TEST_CASE( "mul2 0...4", "[mul2]" ) {
    REQUIRE( mul2(0) == 0 );
    REQUIRE( mul2(1) == 2 );
    REQUIRE( mul2(2) == 4 );
    REQUIRE( mul2(3) == 6 );
    REQUIRE( mul2(4) == 8 );
    // REQUIRE( false );  // 
}

TEST_CASE ("muln 5 5", "[muln]" ) {
    REQUIRE( muln(5,5) == 25 );
}

