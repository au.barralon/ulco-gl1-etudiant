#include "MyWindow.hpp"
#include <glog/logging.h>

int main(int argc, char ** argv) {
     google::InitGoogleLogging(argv[0]);
     LOG(INFO) << "C'est le main" << std::endl;
    Gtk::Main kit(argc, argv);
    MyWindow window;
    kit.run(window);
    return 0;
}

