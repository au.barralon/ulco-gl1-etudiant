#include <iostream>
#include <exception>
#include <vector>

enum class NumError { TooLow, TooHigh, Is37, IsNegative };

// Reads a number in stdin. Throws a NumError if 37 or negative.
int readPositiveButNot37() {
    std::string line;
    std::cout << "enter num: ";
    std::flush(std::cout);
    std::getline(std::cin, line);
    int num = stod(line);
    if (num == 37)
        throw NumError::Is37;
    if(num < 0)
        throw NumError::IsNegative;
    if(num == 0)
        throw NumError::TooLow;
    if(num > 42)
        throw NumError::TooHigh;
    return num;
}

// Reads a number in stdin. Throws a NumError if too low or too high.
std::vector<int> replicate42(int n) {
    return std::vector<int>(n, 42);
}

int main() {

    try {

        // read number
        const int num = readPositiveButNot37();
        std::cout << "num = " << num << std::endl;

        // build vector
        const auto v = replicate42(num);

        // print vector
        for (const int x : v)
            std::cout << x << " ";
        std::cout << std::endl;
    }
    catch (NumError e){
        if(e == NumError::IsNegative){
            std::cerr << "error: negative" << std::endl;
            exit(-1);
        }
        if (e == NumError::TooHigh){
            std::cerr << "error: too high" << std::endl;
            exit(-1);
        }
        if (e == NumError::TooLow){
            std::cerr << "error: too low" << std::endl;
            exit(-1);
        }
        else
            std::cerr << "error" << std::endl;
        exit(-1);
    }

    return 0;
}

