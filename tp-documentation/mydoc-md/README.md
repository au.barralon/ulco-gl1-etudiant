## This is mydoc-md
----

### License
----

This project is under the MIT License (see [LICENSE.txt](link)).

### List
----

   - item 1
   - item 2
   
 ### Table
----

| column 1   | column 2   |
|:----------:|:----------:|
|foo         |bar         |
|toto        |tata        |

### Code
----

```
main :: IO()
main = putStrLn "hello"
```

### Quote
----

> I never said half the crap people said I did.
>
> Albert Einstein

### Image
----

![Description semantic de l'image. Oh...](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/langfr-105px-Visual_Studio_Code_1.35_icon.svg.png)

