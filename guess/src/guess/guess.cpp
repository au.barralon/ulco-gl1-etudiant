#include <guess/guess.hpp>
#include <iostream>
#include <cstdlib>
#include <vector>

int guess::RandomNumber () ///Génère un nombre aléatoire.
{
    srand((unsigned) time(0));
    return rand()%100+1;
}

void guess::afficheHistory() ///Affiche l'historique des nombres que l'utilisateur a saisi.
{
    cout << "history: ";
    if(numbersTried.size() == 0)
    {
        cout << endl;
        return;
    }
    else
    { 
        for(int i = 0; i < numbersTried.size(); i++)
        {
            cout << numbersTried[i] << " ";
        }
        cout << endl;
    }
}

void guess::itsGuessingTime() ///Demande un nombre au joueur et le compare au nombre aléatoire, si le joueur trouve le bon nombre, il gagne.
{
    for(int i = 0; i < 5; i++)
    {
        afficheHistory();
        cout << "number? ";
        cin >> guessingNumber;

        while(guessingNumber > 100 || guessingNumber < 1)
        {
            cout << "invalid" << endl << endl;
            afficheHistory();
            cout << "number? ";
            cin >> guessingNumber;
        }

        if(guessingNumber != randomNumber)
        {
            if(i == 4)
            {
                cout << "lose" << endl << "target: " << randomNumber << endl;
                return;
            }
            if(guessingNumber < randomNumber)
            {
                cout << "too low" << endl << endl;
            }
            if(guessingNumber > randomNumber)
            {
                cout << "too high" << endl << endl;
            }
            numbersTried.push_back(guessingNumber);
        }
        else
        {
            cout << "win" << endl << endl;
            return;
        }
    }
}